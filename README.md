# PseudoRISC Emulator

This is a toy project for the university: It is a minimal implementation of a
RISC-like instruction set, but for a register machine architecture with 256
general purpose registers. It can perform basic integer arithmetic, logical
compares and loops, that's about it. Can still be useful for teaching the
absolute basics in a hands-on class.

## Compile
Based on `cmake`: Run

```
mkdir build && cd build && cmake .. && make
```

This should produce the `pseudorisc` executable.

## Usage
Simple pass the name of the file containing a binary program as first parameter,
e.g.:


```
./pseudorisc ../programs/helloworld.bin
```

## Programming
There is no assembler, see `opcodes.hpp` for the instruction opcodes. All
instructions are of the same length and contain three address code, i.e., encode
destination, first and second operand in the instruction (direct mapping to 256
registers). Unused operands are ignored (I think?).

## Example Programs
In the `programs` subdirectory are some primitive programs: `empty.bin` is the
empty program (does nothing), `helloworld.bin` simply loads the constant 0x2A
into register #0 (sort of "Hello World"), `loop.bin` demonstrates a loop with
fixed bound, `loop2.bin` also a (slightly different) loop and `loop_in.bin`
requires a second input parameter, which is parsed for the loop bound
(via syscall).

Have fun!
Jens
