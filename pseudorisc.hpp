#ifndef __PSEUDO_RISC_HPP__
#define __PSEUDO_RISC_HPP__
#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;

#define MAX_PROGRAM_INSTS						10000					

struct BinOp
{
	opcode_t opcode;
	uint32_t dst;
	uint32_t op1;
	uint32_t op2;

	void dump(ostream &o) const
	{
		o << "Instruction: " << setfill('0')
			<< " opc = 0x" << setw(2) << hex << opcode 
			<< " dst = 0x" << setw(2) << hex << dst
			<< " op1 = 0x" << setw(2) << hex << op1
			<< " op2 = 0x" << setw(2) << hex << op2
			<< endl;
	}
};

struct pseudo_risc_state
{
	uint32_t ip = 0;
	uint32_t program[MAX_PROGRAM_INSTS] = { 0 };
	uint32_t reg[256] = { 0 };
	uint32_t flags;

	void dump(ostream &o) const
	{
		o << "Register File Dump: " << setfill('0') << endl;
		for (int i = 0; i < 32; ++i) {
			for (int j = 0; j < 8; ++j)
				o << "0x" << hex << setw(8) << reg[(i << 3) + j] << " ";
			o << endl;
		}
	}

	void dump_program(ostream &o) const
	{
		o << "Program dump:" << endl << setfill('0') << hex;
		uint32_t ic = 0;
		while (program[ic]) {
			o << "\tInstruction " << ic << ": " << hex << setw(8) << program[ic] << endl;
			++ic;
		}
	}
};

#endif /* __PSEUDO_RISC_HPP__ */
