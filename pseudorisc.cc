#include <sys/stat.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstdint>
#include <cassert>
#include <cstdlib>
#include "opcodes.hpp"
#include "pseudorisc.hpp"
using namespace std;

class InstructionFetch
{
public:
	InstructionFetch(){}
	virtual ~InstructionFetch(){}

	bool instruction_fetch(pseudo_risc_state &state, uint32_t &inst)
	{
		inst = state.program[state.ip];
		++state.ip;
		return inst > 0; // exit on 0
	}
};


class InstructionDecode
{
public:
	InstructionDecode(){}
	virtual ~InstructionDecode(){}

	struct BinOp instruction_decode(pseudo_risc_state &state, uint32_t const &inst)
	{
		struct BinOp op;
		op.op2 = inst & 0xFF;
		op.op1 = (inst & 0xFF00) >> 8;
		op.dst = (inst & 0xFF0000) >> 16;
		op.opcode = static_cast<opcode_t>((inst & 0xFF000000) >> 24);
		return op;
	}
};


class Execute
{
public:
	Execute(){}
	virtual ~Execute(){}

	struct pseudo_risc_state &execute(struct pseudo_risc_state &state, struct BinOp const &op)
	{
		switch (op.opcode) {
		case OP_LDI:
			state.reg[op.dst] = (op.op2 << 8) | op.op1;
			break;

		case OP_ADD:
			state.reg[op.dst] = state.reg[op.op1] + state.reg[op.op2];
			break;
		case OP_SUB:
			state.reg[op.dst] = state.reg[op.op1] - state.reg[op.op2];
			break;
		case OP_MUL:
			state.reg[op.dst] = state.reg[op.op1] * state.reg[op.op2];
			break;
		case OP_DIV:
			if (state.reg[op.op2] == 0) {
				// cerr << "PROGRAM ERROR - DIVISION BY ZERO!" << endl;
				state.program[state.ip] = 0;
				state.flags |= 1 << 1; // signal div by zero
			}
			state.reg[op.dst] = state.reg[op.op1] / state.reg[op.op2];
			break;

		case OP_GOTO:
			state.ip = (op.dst << 16) | (op.op1 << 8) | op.op2;
			break;
		case OP_BEQ:
			state.ip += state.reg[op.op1] == op.op2 ? static_cast<int8_t>(op.dst) : 0;
			break;
		case OP_BNEQ:
			state.ip += state.reg[op.op1] != op.op2 ? static_cast<int8_t>(op.dst) : 0;
			break;
		default:
			// cerr << "EX: Unknown opcode - 0x" << hex << setfill('0') << setw(2) << op.opcode << endl;
			state.program[state.ip] = 0;
			state.flags |= 1; // signal error
		}
		return state;
	}
};


struct pseudo_risc_pipeline
{
	pseudo_risc_state _st;
	InstructionFetch _if;
	InstructionDecode _id;
	Execute _ex;
};

static int print_usage(void)
{
	cerr << "Usage: pseudorisc <path to program> <reg0> <reg1> ... <regN>" << endl << endl;
	return 1;
}

static void load_file(pseudo_risc_state &state, string const &fn)
{
	ifstream f(fn);
	uint32_t ic = 0;
	while (ic < MAX_PROGRAM_INSTS && f.read(reinterpret_cast<char *>(&state.program[ic]), 4))
		++ic;
	f.close();
}

int main(int const argc, char const **argv)
{
	struct pseudo_risc_pipeline risc;
	cout << "This is PseudoRISC 0.1, all your instructions are belong to us."
		<< endl
		<< "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
		<< endl;
	if (argc < 2)
		return print_usage();
	else
		load_file(risc._st, argv[1]);
	
	risc._st.dump_program(cout);

	for (int i = 2; i < argc; ++i)
		risc._st.reg[i-2] = strtoul(argv[i], NULL, 0);
	risc._st.dump(cout);

	// MAIN LOOP
	uint32_t inst;
	while (risc._if.instruction_fetch(risc._st, inst)) {
		struct BinOp op = risc._id.instruction_decode(risc._st, inst);
		risc._st = risc._ex.execute(risc._st, op);
	}

	cout << endl << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
	risc._st.dump(cout);
	if (risc._st.flags) {
		cout << "ABNORMAL TERMINATION: 0x" << hex << setw(8) << setfill('0') << risc._st.flags << endl;
	} else {
		cout << endl << "PseudoRISC finished, bye." << endl;
	}
	return risc._st.flags;
}
