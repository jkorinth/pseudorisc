#ifndef __OPCODE_HPP__
#define __OPCODE_HPP__
#include <cstdint>

enum opcode_t : uint32_t {
	// syscall
	OP_SYS							= 0x00,
	// load & store
	OP_LDI							= 0x01,
	// arithmetic instructions
	OP_ADD							= 0x10,
	OP_SUB							= 0x11,
	OP_MUL							= 0x12,
	OP_DIV							= 0x13,
	// jump instructions
	OP_GOTO							= 0x42,
	OP_BEQ							= 0x43,
	OP_BNEQ							= 0x44,
};

#endif /* __OPCODE_HPP__ */

